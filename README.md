# cocreate-remote

[![Build Status](https://travis-ci.com/snapping-shrimp/cocreate-remote-vscode.svg?branch=main)](https://travis-ci.com/snapping-shrimp/cocreate-remote-vscode)

Cocreate Remote is an Open Source, Self-hosted VS Code Live Share Alternative.
It helps you to **co**llaborate and **create** with others

## Contributing

Want to contribute? Yay! Awesome! Read the
[contributing guide](CONTRIBUTING.md) for information on how to contribute to
this project. If you have any questions related to contributing or if you need
any help, just raise a GitLab issue or reach out to the maintainers directly! :)
