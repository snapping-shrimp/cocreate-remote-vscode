# Contributing

## Install Node.js and npm

To start contributing to this project, first install
[`Node.js`](https://nodejs.org/en/) `12.19.0+` and `npm` `6.14.8+`.

You can use something like [`nvm`](https://github.com/nvm-sh/nvm) to manage
multiple `Node.js` versions in your system for different projects

## Fork and clone repo

[Fork](https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/forks/new)
this repo and then clone it

```bash
$ git clone https://gitlab.com/<your-username>/cocreate-remote-vscode
$ # OR use ssh
$ git clone git@gitlab.com:<your-username>/cocreate-remote-vscode.git
$ cd cocreate-remote-vscode
$ # add the upstream git repository
$ git remote add upstream git@gitlab.com:snapping-shrimp/cocreate-remote-vscode.git
```

## Download dependencies

```bash
$ npm install
```

## Running and debugging the extension

Install VS Code Stable version `1.50.1+` and open this project and run the
config named `Run Extension` and it will run the extension in a new
window. This is actually running the extension in debug mode, so you can already
start debugging by just adding breakpoints

## Compiling

To compile the code

```bash
$ npm run compile
```

## Running and debugging unit tests

To run the unit tests with coverage

```bash
$ npm run test:unit
```

For development, it's easier to run tests in watch mode to continuously watch
test files and run tests when the test files change

```bash
$ npm run test:unit:watch
```

You can also run unit tests within VS Code without using the terminal. We
recommend you to try out the
[Jest extension](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest). It can help with running Jest tests, debugging them and also with
showing code coverage. If you open this project in VS Code, you will get this
extension as a recommendation among other recommendations

If you are using other text editors or IDEs, they too might have extensions or
plugins for Jest in their marketplace, with similar features.

## Running and debugging integration test

Open this project in VS Code and run the debug config named `Extension tests`
and it will run the extension integration tests in a new window

This is running the integration test in debug mode, so you can already start
debugging by just adding breakpoints

You can also run the integration test from the command line (CLI) by running
this

```bash
$ # this will compile the code
$ # and run integration tests
$ npm run test:integration
```

## Running lint

To check the lint and formatting issues and to fix them just use these commands

```bash
$ # checking
$ npm run lint
$ # automated fixing wherever possible
$ npm run lint:fix
```

If automated fix is not possible in some cases, you would have to intervene and
manually fix the issue by changing the code. Check the
[docs about linting and formatting](./docs/linting-and-formatting.md) if you
need more information

## Install VS Code extensions for ease of development

We recommend installing a few extensions for ease of development. These are
present in the `.vscode/extensions.json` file. VS Code will read this file
and show you recommendations :) If not, use the extension IDs in the JSON file
to search the extensions in the marketplace and then install it :)

## Contributing to the project

- Commit all your changes in a separate branch and not the `main` branch
- Follow Test Driven Development (TDD) as much as possible. We at least
  expect tests for all features and bug fixes. This is for easy maintenance. This
  is also to ensure we do not break the code you write, for which you put a lot
  of effort. Check the [writing tests doc](docs/testing/writing-tests.md) to know
  how to write tests for this project
- Have as small and logical commits as possible
- Be up to date with `main` branch by rebasing your branch with `main` like this

```bash
$ # be on main branch
$ git checkout main
$ # pull the latest from the main branch of the upstream,
$ # that is, the original repo
$ git pull upstream main
$ # push the changes in main branch to your fork
$ # to keep your fork up to date
$ git push origin main
$ # be on your separate feature or issue branch
$ git checkout <feature-or-issue>
$ # rebase your feature or issue branch on top of main branch
$ git rebase main
```

You can read more about `rebase` in
[Atlassian Git Rebase tutorial](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase)

- Raise a [Merge Request (MR)](https://docs.gitlab.com/ee/user/project/merge_requests/#creating-merge-requests) once your are done building :)
- Make sure the Continuous Integration (CI) pipeline passes - it should be
  green. Check more about the CI [here](docs/continuous-integration.md)

To ensure that the checks and validations in CI are also run in local, the
project has `pre-push` git hook which runs for every `git push` command. This
way you get faster feedback on issues in your local machine instead of pushing
and then waiting for the CI to run and then fail and show you what went wrong.
That's a longer feedback cycle to get feedback on what went wrong.
