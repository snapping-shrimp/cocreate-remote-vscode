# Pre Release Handbook

Before every release, below are the following things that need to be checked for
a good and smooth release without errors

- Updated release notes in the `README.md`
- Updated `CHANGELOG.md`
- Updated demos (GIFs) of new features
- Continuous Integration pipeline is green, as always
- One round of manual testing as a manual sanity check
