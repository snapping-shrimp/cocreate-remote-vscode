# Acceptance Criteria

For every user story (feature) or issue that the developer works on, there must
be some acceptance criteria, which decides if the work is completely done or
not. Apart from this acceptance criteria, which specific to the thing that the
developer worked, there are some general things that must be followed

## General Acceptance Criteria

### Tests

Are there enough tests for the feature or issue? What are the different kind of
tests that has been done?

Usually

- Unit tests - a lot
- Integration tests - a few
- End to End tests - very very few

For every feature or issue, there will be a testing tracker sheet / list, which
talks about the different kind of testing that needs to be done to consider the
work as completely done.

Also, mention in the card if there are any manual tests that were done. Ideally
there must be as little manual tests as possible - this way, releasing software
is very easy - as easy as running all the automated tests and then releasing the
software with confidence. Some basic manual testing can be done at the end just
for a test drive, more like exploratory test. This kind of exploratory manual
testing can be done even during development - this is done to find blindspots
that we might have missed - missed testing some scenario even though it's
working, or it's not working and you need to write test and also fix the it.

### Documentation

If it's a new feature, how is the documentation related to the feature

#### Development

##### Documentation for code written

If new internal modules have been created for the feature, are they readable?
And are there documentation around the methods and the module in general - in
the form of comments? Usually tests are documentation, yes, but methods can have
a basic description and/ an example. As names are only so good sometimes. 😅
After that you gotta resort to tests and just before that, the basic description
can help :)

##### Documentation for local development

If new tools have been added or if the workflow related to development has
changed, then it has to be updated, so that new contributors of the project can
find the latest and updated documentation to get started on contributing to the
project

#### End users

##### Demos using Images

For end users to know about new features, are there demos in the form of GIFs
to show the end user how the feature looks like? This is especially important if
the feature is complete and there's nothing more to develop as part of the
feature in the upcoming release. Or else wait for a logical end for the feature
to capture demos in the form of GIFs

##### Release notes and Changelog

Some basic release notes should be added in the `README.md` to talk about the
feature. Do the same in `CHANGELOG.md`

##### Features

High level mention of the features in the `README.md`

##### Requirements

Any explicit requirements in the `README.md`, in case any feature needs
anything external.

##### Extension Settings / Configuration

Add any settings that can be done in the extension in the `README.md` so that
the user is aware about it

### Code

#### Complexity

Code should be as simple as possible. Strive for simplicity. Simple does not
have to mean easy. But it would be great if it is simple AND easy, or at least
simple!

#### Deprecated APIs

Code should not be using deprecated APIs. It's better to move to newer APIs
which will have better support in the future, also because deprecated APIs
might get removed in the future in some newer version, like major version. This
primarily applies to library APIs.
