# Linting and Formatting

To ensure that the code is consistent in the whole codebase and to catch code
quality issues, we have added linting and formatting tools for checking of
issues and for some automated fixing of such issues too

## Linting

We use [ESLint](https://eslint.org) for linting **only**. We try to catch
code quality issues and try to ensure that the code quality is good based on
some rules. The linting is completely automated, along with fixing in simple
cases. Manual intervention to fix is needed only in a few cases

We recommend installing VS Code Extension for ESLint as per the project
extensions recommendation, so that you can have a good experience while
developing. If you are using a different editor or IDE, choose an appropriate
ESLint extension or plugin

## Formatting

We use a combination of [Prettier](https://prettier.io) and
[EditorConfig](https://editorconfig.org/) to ensure that everyone can easily
write code and not worry about code format, like space vs tabs, how many spaces
or how many tabs, max length for every line, or line endings in windows, and
many other issues related to formatting. Like linting, this too is automated -
both checking and fixing.

We recommend installing VS Code Extension for Prettier and EditorConfig as per
the project extensions recommendation, so that you can have a good experience
while developing. If you are using a different editor or IDE, choose an
appropriate Prettier and EditorConfig extension or plugin.
