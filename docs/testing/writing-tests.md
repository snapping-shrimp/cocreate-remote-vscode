# Writing Tests

## Unit Testing

This project uses [Jest](https://jestjs.io) as the unit testing framework.

You can look at any file with the extension `.test.ts` to understand how to write tests in Jest. There are also tons of tutorials online about how to write tests in Jest. If you are not sure about how to approach and test the feature or bug fix you are working, please ping the maintainers and they will be able to help you! :)

Always use the file extension `.test.ts` for your unit test files

Jest has some global functions defined, like `describe`, `test`, which you can
use to write the unit tests.

All tests are written in TypeScript.
[`ts-jest`](https://kulshekhar.github.io/ts-jest) is used to run Jest tests
written in TypeScript. It does type checking on the test files, so ensure you
don't have any type errors in your test files too! :)

The configuration for Jest is present at `jest.config.js`.

When you run `tsc` or `npm run compile`, it does NOT compile the Jest test
files, that is, your unit test files. You just need to write your tests in
TypeScript and run `jest` and it will take care of everything

## Integration Testing

This project uses [Mocha](https://mochajs.org) as the testing framework to run
integration tests.

You can find integration tests in files with the extension `.vsctest.ts`.
`vsctest` is the short form for - **VS** **C**ode **Test**.

Always use the file extension `.vsctest.ts` for your integration test files

Mocha has some global functions defined, like `suite`, `test`. But it's better
to use explicit imports and use the functions from the imports. This is for
better IDE or text editor support and for safety too. Look at
[Type Conflict Issue](global-type-conflict-issue) to understand more. You can
just use code like below

```typescript
import * as m from "mocha";

m.suite("Extension Test Suite", () => {
  m.test("Sample test", () => {
    assert.strictEqual(-1, [1, 2, 3].indexOf(5));
    assert.strictEqual(-1, [1, 2, 3].indexOf(0));
  });
});
```

Note how `mocha` library has been imported to use `m.suite` and `m.test`,
instead of using `suite` and `test` global functions.

You cannot use the following Mocha global functions

```
beforeEach
afterEach
describe
xdescribe
it
test
xit
```

The integration tests run in a different environment - inside the VS Code
instance. The tests run programmatically using `mocha`.

The integration test TypeScript files are all compiled with `tsc` when running
`npm run compile`.
