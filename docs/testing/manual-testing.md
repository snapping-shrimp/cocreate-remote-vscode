# Manual Testing

Every time some sort of manual testing is done, note it down as a scenario. It's
okay if we are not able to automate the test - as long as we have the steps on
how to do it manually, we can then later put more effort and automate it. This
is key to automating the testing - we need the right steps that was done
manually to test it.

Any day, automated testing is better as it's faster and requires no effort from
the developers once the right and good tests are written.
