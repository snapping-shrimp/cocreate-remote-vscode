## Global Type Conflict Issue

This project uses both Jest and Mocha testing frameworks. Given this is a
TypeScript project, the type definitions of the two packages had to be imported.
This lead to an issue of conflicting global types, as both Jest and Mocha
have quite some global types and some of them have same names but different
definitions. Hence the conflict, which is thrown as an error by the TypeScript
compiler.

To overcome this issue, the project does NOT import `@types/mocha` and instead
maintains it's own copy of `mocha` package type definitions in the
`types-overrides/mocha/index.d.ts` file. The file is a copy of the type
definition file from the `@types/mocha` package, but some of the global types
that conflict with Jest are commented out.

### Problems

#### Maintenance

The custom mocha type definition file is an extra maintenance for the project,
yes.

We hope that there won't be much changes in the mocha project in the near
future, at least with the type definitions. And we will be careful with the
upgrades in mocha library! :)

#### Possible Type Errors

Integration test TypeScript files are run by the TypeScript compiler. If you
are using global functions, then, in some cases, you might use a Jest global
function by mistake - because both Jest and Mocha have the function but we
include only Jest global function type defintions when there's a tie.

The tie occurs for the following global functions

```
beforeEach
afterEach
describe
xdescribe
it
test
xit
```

The above functions are NOT available from mocha. It is ONLY available from
Jest.

When using Jest global function by mistake with Mocha global function syntax,
TypeScript is going to throw a type error in case of type issues, as we have
hidden some of the mocha global function definitions from it.

We recommend you to write integration tests by importing mocha functions and
not using global functions, to avoid any possible type errors, especially when
the above mentioned functions are needed.

#### IDE and Editor Support

The IDE or text editors you use will not be able to help you with writing mocha
tests in case you want to use the above globals. For example the commonly
used `test`. The IDE or text editor will think that `test` belongs to Jest and
show documentation for that and errors and help based on the Jest type
definition.

It's better to import mocha and use the functions in it

For better IDE and text editor support, we recommend you to write integration
tests by importing mocha functions and not using global functions.
