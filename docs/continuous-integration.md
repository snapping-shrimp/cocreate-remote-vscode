# Continuous Integration

This project uses Continuous Integration (CI) to make sure that the automated
tests setup are always run and are passing so that we can delivery quality
software. The CI also ensures there's no "works on my machine" problems

We use Travis CI for our unit tests and for running integration tests in Linux
and Mac OS. For running integration tests in Windows OS, we use AppVeyor.

You can find the configuration for Travis CI config at `.travis.yml` and for
AppVeyor at `appveyor.yml`

The CI pipeline runs for `main` branch and for Pull Requests / Merge Requests
too, so that every change is first tested on CI before merging with the `main`
branch.

We encourage everyone to strive to keep the CI pipeline green. If you need any
help, you can ask the maintainers :)
