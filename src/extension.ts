import * as vscode from "vscode";

export function activate(context: vscode.ExtensionContext): void {
  console.log("cocreate-remote extension is now active");

  const disposable = vscode.commands.registerCommand(
    "cocreate-remote.signin.github",
    () => {
      vscode.authentication
        .getSession("github", [], {
          createIfNone: true,
        })
        .then(
          (authSession) => {
            vscode.window.showInformationMessage(
              `Signed in as: ${authSession.account.label} (github)`
            );
          },
          (err) => {
            vscode.window.showErrorMessage(`Error occured: ${err}`);
          }
        );
    }
  );

  context.subscriptions.push(disposable);
}
